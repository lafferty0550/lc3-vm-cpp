#include <iostream>
#include <cstdio>
#include <csignal>
#include <fstream>

#include <termios.h>

#include "LC-3.h"

using namespace std;

static struct termios original_io_;

void LC3::IO::disable_input_buffering() {
    tcgetattr(0, &original_io_);
    struct termios new_tio = original_io_;
    new_tio.c_lflag &= ~ICANON & ~ECHO;
    tcsetattr(0, TCSANOW, &new_tio);
}

void LC3::IO::restore_input_buffering() {
    tcsetattr(0, TCSANOW, &original_io_);
}

void LC3::IO::handle_interrupt(const int signal) {
    restore_input_buffering();
    cout << endl;
    exit(signal);
}

uint16_t LC3::IO::check_key() {
    fd_set readfd;
    FD_ZERO(&readfd);
    FD_SET(0, &readfd);

    struct timeval timeout = {0, 0};
    return select(1, &readfd, nullptr, nullptr, &timeout) != 0;
}

LC3::LC3() {
    this->reg_[Registers::R_PC] = 0x3000;
}

void LC3::run() {
    signal(SIGINT, IO::handle_interrupt);
    IO::disable_input_buffering();
    atexit(IO::restore_input_buffering);
    bool running = true;
    while (running) {
        uint16_t instr = this->mem_read_(this->reg_[Registers::R_PC]++);
        this->handle_instr_(instr, running);
    }
}

uint16_t LC3::sext_(uint16_t x, const uint8_t bit_count) const {
    if ((x >> (bit_count - 1)) & 0b1)
        x |= (0xFFFF << bit_count);
    return x;
}

void LC3::mem_write_(const uint16_t addr, const uint16_t val) {
    memory_[addr] = val;
}

uint16_t LC3::mem_read_(const uint16_t addr) {
    if (addr == MMR::KBSR) {
        if (LC3::IO::check_key()) {
            memory_[MMR::KBSR] = (1 << 15);
            memory_[MMR::KBDR] = getchar();
        } else
            memory_[MMR::KBSR] = 0;
    }
    return memory_[addr];
}

void LC3::update_flags_(const uint16_t r) {
    if (reg_[r] == 0)
        reg_[Registers::R_COND] = Flags::FL_ZER;
    else if (reg_[r] >> 15)
        reg_[Registers::R_COND] = Flags::FL_NEG;
    else
        reg_[Registers::R_COND] = Flags::FL_POS;
}

void LC3::handle_instr_(const uint16_t instr, bool &running) {
    uint8_t op = instr >> 12;
    switch (op) {
        case Opcodes::ADD: {
            uint8_t dr = (instr >> 9) & 0b111;
            uint8_t sr1 = (instr >> 6) & 0b111;
            uint8_t imm_flag = (instr >> 5) & 0b1;
            if (!imm_flag) {
                uint8_t sr2 = instr & 0b111;
                reg_[dr] = reg_[sr1] + reg_[sr2];
            } else
                reg_[dr] = reg_[sr1] + sext_((instr & 0x1F), 5);
            update_flags_(dr);
            break;
        }
        case Opcodes::AND: {
            uint8_t dr = (instr >> 9) & 0b111;
            uint8_t sr1 = (instr >> 6) & 0b111;
            uint8_t imm_flag = (instr >> 5) & 0b1;
            if (!imm_flag) {
                uint8_t sr2 = instr & 0b111;
                reg_[dr] = reg_[sr1] & reg_[sr2];
            } else
                reg_[dr] = reg_[sr1] & sext_((instr & 0x1F), 5);
            update_flags_(dr);
            break;
        }
        case Opcodes::NOT: {
            uint16_t dr = (instr >> 9) & 0b111;
            uint16_t src = (instr >> 6) & 0b111;
            reg_[dr] = ~reg_[src];
            update_flags_(dr);
            break;
        }

            // PC-relative addressing mode
        case Opcodes::LD: {
            uint16_t dr = (instr >> 9) & 0b111;
            uint16_t offset = instr & 0x1FF;
            reg_[dr] = mem_read_(reg_[Registers::R_PC] + sext_(offset, 9));
            update_flags_(dr);
            break;
        }

        case Opcodes::ST: {
            uint16_t src = (instr >> 9) & 0b111;
            uint16_t offset = instr & 0x1FF;
            mem_write_(reg_[Registers::R_PC] + sext_(offset, 9), reg_[src]);
            break;
        }

            // Indirect addressing mode
        case Opcodes::LDI: {
            uint16_t dr = (instr >> 9) & 0b111;
            uint16_t offset = instr & 0x1FF;
            reg_[dr] = mem_read_(
                    mem_read_(reg_[Registers::R_PC] + sext_(offset, 9)));
            update_flags_(dr);
            break;
        }

        case Opcodes::STI: {
            uint16_t src = (instr >> 9) & 0b111;
            uint16_t offset = instr & 0x1FF;
            mem_write_(mem_read_(reg_[Registers::R_PC] + sext_(offset, 9)),
                       reg_[src]);
            break;
        }

            //Base + offset addressing mode
        case Opcodes::LDR: {
            uint16_t dr = (instr >> 9) & 0b111;
            uint16_t base = (instr >> 6) & 0b111;
            uint16_t offset = instr & 0b11111;
            reg_[dr] = mem_read_(reg_[base] + sext_(offset, 5));
            update_flags_(dr);
            break;
        }

        case Opcodes::STR: {
            uint16_t src = (instr >> 9) & 0b111;
            uint16_t base = (instr >> 6) & 0b111;
            uint16_t offset = instr & 0b11111;
            mem_write_(reg_[base] + sext_(offset, 5), reg_[src]);
            break;
        }

            //Load effective address
        case Opcodes::LEA: {
            uint16_t dr = (instr >> 9) & 0b111;
            uint16_t offset = instr & 0x1FF;
            reg_[dr] = reg_[Registers::R_PC] + sext_(offset, 9);
            update_flags_(dr);
            break;
        }

        case Opcodes::BR: {
            uint8_t flag = (instr >> 9) & 0b111;
            uint16_t offset = instr & 0x1FF;
            if (flag & reg_[Registers::R_COND])
                reg_[Registers::R_PC] += sext_(offset, 9);
            break;
        }
        case Opcodes::JSR: {
            uint16_t dr = (instr >> 6) & 0b111;
            uint16_t long_pc_offset = sext_(instr & 0x7FF, 11);
            uint16_t long_flag = (instr >> 11) & 0b1;

            reg_[Registers::R_R7] = reg_[Registers::R_PC];
            if (long_flag)
                reg_[Registers::R_PC] += long_pc_offset;
            else
                reg_[Registers::R_PC] = reg_[dr];
            break;
        }
        case Opcodes::JMP: {
            uint8_t base = (instr >> 6) & 0b111;
            reg_[Registers::R_PC] = reg_[base];
            break;
        }
        case Opcodes::TRAP: {
            switch (instr & 0xFF) {
                case Traps::GETC: {
                    reg_[Registers::R_R0] = (uint16_t) getchar();
                    break;
                }
                case Traps::OUT: {
                    cout << (char) reg_[Registers::R_R0];
                    fflush(stdout);
                    break;
                }
                case Traps::PUTS: {
                    uint16_t *c = memory_ + reg_[Registers::R_R0];
                    while (*c) {
                        cout << (char) *c;
                        ++c;
                    }
                    fflush(stdout);
                    break;
                }
                case Traps::IN: {
                    cout << "Enter a character: ";
                    char c;
                    cin >> c;
                    cout << c;
                    reg_[Registers::R_R0] = (uint16_t) c;
                    break;
                }
                case Traps::PUTSP: {
                    uint16_t *c = memory_ + reg_[Registers::R_R0];
                    while (*c) {
                        char char1 = (*c) & 0xFF;
                        cout << char1;
                        char char2 = (*c) >> 8;
                        if (char2)
                            cout << char2;
                        ++c;
                    }
                    fflush(stdout);
                    break;
                }
                case Traps::HALT: {
                    cout << "HALT" << endl;
                    running = false;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

int LC3::read_image(const char *path) {
    fstream file(path);
    if (!file.is_open())
        return 0;
    uint16_t origin;
    file.read(reinterpret_cast<char *>(&origin), sizeof(origin));
    origin = (origin << 8) | (origin >> 8);
    uint16_t max_read = UINT16_MAX - origin;
    uint16_t *p = memory_ + origin;
    file.read(reinterpret_cast<char *>(p), max_read);
    streamsize bytes = file.gcount() / 2;
    while (bytes-- > 0) {
        *p = (*p << 8) | (*p >> 8);
        ++p;
    }
    file.close();
    return -1;
}