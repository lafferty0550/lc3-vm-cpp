#ifndef SRC_LC_3_H_
#define SRC_LC_3_H_

#include <cstdint>

class LC3 {
public:
    LC3();

    void run();

    int read_image(const char *path);

private:
    [[nodiscard]] uint16_t mem_read_(uint16_t addr);

    void mem_write_(uint16_t addr, uint16_t val);

    void handle_instr_(uint16_t instr, bool &running);

    void update_flags_(uint16_t r);

    [[nodiscard]] uint16_t sext_(uint16_t x, uint8_t bit_count) const;

    enum Registers {
        R_R0,
        R_R1,
        R_R2,
        R_R3,
        R_R4,
        R_R5,
        R_R6,
        R_R7,
        R_PC,
        R_COND,
        R_COUNT
    };

    enum Opcodes {
        // operate
        ADD = 0b0001,
        AND = 0b0101,
        NOT = 0b1001,

        // data movement
        LD = 0b0010,
        LDI = 0b1010,
        LDR = 0b0110,
        LEA = 0b1110,
        ST = 0b0011,
        STR = 0b0111,
        STI = 0b1011,

        // control
        BR = 0b0000,
        JSR = 0b0100,
        JMP = 0b1100,
        TRAP = 0b1111
    };

    enum Flags {
        FL_POS = 1 << 0,
        FL_ZER = 1 << 1,
        FL_NEG = 1 << 2
    };

    enum Traps {
        GETC = 0x20,
        OUT = 0x21,
        PUTS = 0x22,
        IN = 0x23,
        PUTSP = 0x24,
        HALT = 0x25
    };

    enum MMR {
        KBSR = 0xFE00,
        KBDR = 0xFE02
    };

    uint16_t reg_[Registers::R_COUNT] = {0};
    uint16_t memory_[UINT16_MAX] = {0};

    class IO {
    public:
        static void handle_interrupt(int signal);

        static void restore_input_buffering();

        static void disable_input_buffering();

        static uint16_t check_key();
    };
};


#endif
