#include <iostream>

#include "LC-3.h"

using namespace std;

int main(int argc, const char **argv) {
	LC3 emulator;

	if (argc < 2) {
		cout << "Please point out the file.\n";
		exit(EXIT_FAILURE);
	}
	for (int i = 1; i < argc; ++i)
		if (!emulator.read_image(argv[i])) {
			cout << "Failed to load image.\n";
			exit(EXIT_FAILURE);
		}

	emulator.run();
	return 0;
}
